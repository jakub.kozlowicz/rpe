# Lecture 5 - Simulation and libraries

## Simulation

### What is simulation?

- A simulation imitates the behavior of a system with the use of a model
- The models represents behavior of the specific part or process of the system
- The simulation presents how the model evolves under different conditions over time

### Why do we need simulation?

- To test the system before it is built
- Less financial risk
- Repeated testing in the same conditions
- To generate synthetic data
- Examine long-term behavior

### Robots simulation

- Simulation of the robot behavior
- Simulation of the environment
- Model, analyze and optimize the robot behavior before physical implementation
- The process of mimicking the behavior of a real system with the use of a model

### Applications of robotics simulation

- Algorithm development
- Prototype testing
- Training (like machine learning with synthetic data)
- Verification of the system
- Validation of the system

### Key components of robotic simulation

- Robot model: Mathematical representation of the robot.
- Environment model: Virtual representation of the environment.
- Sensor model: Emulation of the sensors, imus, cameras, etc.
- Physics engine: Simulation software that calculates the motion of the robot
and the environment.

### Physics engine

- A physics engine is a software component that provides a simulation of physical systems.
- It is used to create a virtual environment with the use of the laws of physics.
- Components:
  - Collision detection
  - Rigid body dynamics
  - Soft body dynamics
  - Integration Models
- Popular engines:
  - Bullet Physics
  - ODE (Open Dynamics Engine)
  - PhysX
- Challenges: Overcoming computational complexity and accuracy
- Future developments: Trends include improved real-time simulations and AI integration.

### Popular robotics simulators

- Gazebo: Open source, multi-robot simulator with physics engine
- Nvidia Isaac Sim: A simulation environemt focused on the physics simulation and accelerated by Nvidia GPUs.
- O3DE (Open 3D Engine): General purpose robotics simulator.
- V-REP (CoppeliaSim): Vesatile robot experimation platform.
- Webots: Open source, multi-robot simulator with physics engine
- CARLA: Open source, multi-robot simulator with physics engine
- Unity: Proprietary, multi-robot simulator with physics engine

#### Gazebo

- Gazebo is a multi-robot simulator with physics engine
- It is open source and free
- It is used in the development of robots and autonomous systems
- 3D visualization for modeling and testing robot designs
- Extensive library of sensors, objects and materials
- TCP/IP interface for easy integration with other software

## URDF

### What is URDF?

- URDF (Unified Robot Description Format) is an XML format for representing a robot model
- It is used to describe the kinematic and dynamic properties of a robot

### Features

- Standardization: Provides a standard way to describe a robot
- Interoperability: It is used by many robotics software
- Simulations: Facilities accurate simulation of robot behavior

### URDF elements

- Link: A rigid body with inertia, visual and collision properties
- Joint: A connection between two links
- Material: Visual properties of a link
- Transmission: A mechanism that transmits force and motion

### URDF example

```xml
<?xml version="1.0"?>
<robot name="my_robot">
  <link name="base_link">
    <visual>
      <geometry>
        <box size="0.1 0.1 0.1"/>
      </geometry>
    </visual>
    <collision>
      <geometry>
        <box size="0.1 0.1 0.1"/>
      </geometry>
    </collision>
    <inertial>
      <mass value="1"/>
      <inertia ixx="1" ixy="0" ixz="0" iyy="1" iyz="0" izz="1"/>
    </inertial>
  </link>
  <link name="link1">
    <visual>
      <geometry>
        <box size="0.1 0.1 0.1"/>
      </geometry>
    </visual>
    <collision>
      <geometry>
        <box size="0.1 0.1 0.1"/>
      </geometry>
    </collision>
    <inertial>
      <mass value="1"/>
      <inertia ixx="1" ixy="0" ixz="0" iyy="1" iyz="0" izz="1"/>
    </inertial>
  </link>
  <joint name="joint1" type="revolute">
    <parent link="base_link"/>
    <child link="link1"/>
    <origin xyz="0 0 0" rpy="0 0 0"/>
    <axis xyz="0 0 1"/>
    <limit effort="100" lower="-3.14" upper="3.14" velocity="1"/>
  </joint>
</robot>
```

## ODE solving

### Definition

- Ordinary differential equation (ODE) is an equation that contains a function of one independent variable and its derivatives with respect to that variable.

### Example

```
dy/dx = f(x, y)
```

### Importance

- ODEs are used to model many phenomena in science and engineering
- They are used to model the motion of a physical system
- They are used to model the change in a biological population

### Application in robotics

The dynamics model of the underwater unmanned vehicle is given by the following ODE:

```
M(q) * q'' + C(q, q') * q' + G(q) = τ
```

where:

- `q` is the vector of generalized coordinates
- `q'` is the vector of generalized velocities
- `q''` is the vector of generalized accelerations
- `M(q)` is the inertia matrix
- `C(q, q')` is the Coriolis and centrifugal matrix
- `G(q)` 
- `τ` is the vector of generalized forces

### Solving ODEs with SciPy

- Features:
  - Open source
  - Provides the `solbe_ivp` function
    - Provides a variety of ODE solvers
    - Provides a variety of integration methods
    - Provides the `odeint` function
- 


### Example of ODE solving

```python
import numpy as np
from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt

def f(t, y):
    return -y + t + 1

t_span = [0, 5]
y0 = [0]
t_eval = np.linspace(0, 5, 100)

sol = solve_ivp(f, t_span, y0, t_eval=t_eval)

plt.plot(sol.t, sol.y[0])
plt.xlabel('t')
plt.ylabel('y')
plt.show()
```

## Optimization

### Definition

Optimization - The act of making something as good or effective as possible.

Types of optimization:

- Unconstrained optimization
- Constrained optimization
- Global optimization
- Local optimization

Algorithms:

- Gradient descent
- Genetic algorithms
- Simulated annealing
- Particle swarm optimization

Applications:

- Machine learning
- Finance
- Engineering

Challenges:

- Non-convexity
- High dimensionality

Popular optimization tools:

- Ceres Solver
- g2o
- CasADi

### Ceres

Introduction:

- Ceres Solver is an open source C++ library for modeling and solving large, complicated optimization problems.
- It is used in robotics, computer vision, machine learning and other fields.

Features:

- Non-linear least squares
- Non-linear least absolute deviations
- Non-linear robust cost functions

Applications:

- Robotics: SLAM, sensor calibration, structure from motion
- Computer vision: Structure from motion, bundle adjustment, stereo reconstruction
- Machine learning: Maximum likelihood estimation, maximum a posteriori estimation

### g2o

- g2o is an open source C++ framework for optimizing graph-based nonlinear error functions.
- It is used in robotics, computer vision, machine learning and other fields.

general graph optimization framework that readily allows for the optimization of a wide range of problems in robotics and computer vision.

Features:

- Simultaneous localization and mapping (SLAM)
- Bundle adjustment
- Structure from motion
- Multi-body pose estimation

### CasADi

- CasADi is an open source C++ library for numerical optimization.
- It is used in robotics, computer vision, machine learning and other fields.

Features:

- Symbolic calculations
- Algorithmic differentiation
- Interfaces

Applications:

- Optimal control
- Nonlinear programming
- Robotics
- System identification

## Linear algebra

### What is Eigen?

- Eigen is an open source C++ library for linear algebra
- Designed for high-performance matrix and vector operations.

### Features

- Expressiveness: Intuitive syntax for linear algebra operations
- Performance: Optimized for vectorization and parallelization
- Versatility: Supports dense and sparse matrices
- Portability: Supports many platforms and operating systems

### Applications

- Robotics: Kinematics, dynamics, control
- Computer vision: Structure from motion, bundle adjustment, stereo reconstruction
- Machine learning: Principal component analysis, linear regression, neural networks