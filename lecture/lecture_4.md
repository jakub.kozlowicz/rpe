# Lecture 4 - ROS2: mechanism, tools

## Communication mechanism

### Services

- Request-response model (one0to-one)
- Synchronous
- Services defined in `.srv` files
- Underlying layer: DDS
- Standard services: `std_srvs` package

![service](https://docs.ros.org/en/foxy/_images/Service-SingleServiceClient.gif)

#### Example

**AddTwoInts.srv**

```text
int64 a
int64 b
---
int64 sum
```

#### Example of the service server

```python
import rclpy
from rclpy.node import Node
from example_interfaces.srv import AddTwoInts

class AddTwoIntsServer(Node):

    def __init__(self):
        super().__init__('add_two_ints_server')
        self.srv = self.create_service(AddTwoInts, 'add_two_ints', self.add_two_ints_callback)

    def add_two_ints_callback(self, request, response):
        response.sum = request.a + request.b
        self.get_logger().info('Incoming request\na: %d b: %d' % (request.a, request.b))
        return response


def main(args=None):
    rclpy.init(args=args)
    add_two_ints_server = AddTwoIntsServer()
    rclpy.spin(add_two_ints_server)
    rclpy.shutdown()


if __name__ == '__main__':
    main()
```

#### Example of the service client

```python
import rclpy
from rclpy.node import Node
from example_interfaces.srv import AddTwoInts


class AddTwoIntsAsyncClient(Node):

    def __init__(self):
        super().__init__('add_two_ints_client')
        self.cli = self.create_client(AddTwoInts, 'add_two_ints')
        while not self.cli.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('service not available, waiting again...')
        self.req = AddTwoInts.Request()

    def send_request(self):
        self.req.a = int(input("Type the first number: "))
        self.req.b = int(input("Type the second number: "))
        self.future = self.cli.call_async(self.req)


def main(args=None):
    rclpy.init(args=args)
    add_two_ints_client = AddTwoIntsAsyncClient()
    add_two_ints_client.send_request()
    while rclpy.ok():
        rclpy.spin_once(add_two_ints_client)
        if add_two_ints_client.future.done():
            try:
                response = add_two_ints_client.future.result()
            except Exception as e:
                add_two_ints_client.get_logger().info(
                    'Service call failed %r' % (e,))
            else:
                add_two_ints_client.get_logger().info(
                    'Result of add_two_ints: for %d + %d = %d' %
                    (add_two_ints_client.req.a, add_two_ints_client.req.b, response.sum))
            break
    add_two_ints_client.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
```

### Actions

- Request-response model (one-to-one)
- Functionality similar to services
- Preemptable (cancelable)
- Provides feedback during execution
- An action client send a goal to an action server that acknowledges the goal
  and return the feedback and a result

![action](https://docs.ros.org/en/foxy/_images/Action-SingleActionClient.gif)

### Comparison

Topics:

- Purpose: continuous data stream
- Many to many connection
- Data might be published and subscribed independently

Services:

- Purpose: remote procedure call that can be executed quickly
- One to one connection

Actions:

- Purpose: remote procedure call that can be executed for a long time
- One to one connection (can be preempted)

## Nodes parametrization

- Supported types:
  - `bool`
  - `int`
  - `float`
  - `string`
- Defined in `.yaml` files
- Can be set in launch files
- Node specific (different from ROS1 where there was a parameter server)

### Useful cli commands

- `ros2 param list`
- `ros2 param get <node_name> <param_name>`
- `ros2 param set <node_name> <param_name> <value>`

### Example

**params.yaml**

```yaml
my_node:
  ros__parameters:
    my_bool_param: true
    my_int_param: 42
    my_float_param: 3.14
    my_string_param: "Hello world!"
```

## Launchers

- Can be written in Python or XML or YAML
- Can be used to start multiple nodes at once
- It's a script to run whole robotic system, so it allows starting multiple
  nodes with one command instead of starting each node manually
- The launch system in ROS2 is responsible for starting nodes, setting
  parameters, remapping topics, etc.
- CLI: `ros2 launch <package_name> <launch_file_name>`

### Example

**my_launch.py**

```python
from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription(
        [
            Node(
                package='demo_nodes_cpp',
                executable='talker',
                name='talker',
                parameters=[{'my_param': 'Hello world!'}]
            ),
            Node(
                package='demo_nodes_cpp',
                executable='listener',
                name='listener',
                output='screen'
            )
        ]
    )
```

## Executors

Uses the underlying system resources to invoke callbacks
of subscription, timers, service servers, etc. on incoming
messages and events.

- Responsible for executing callbacks
- Single threaded executor: `rclpy.spin(node)`
- Multi threaded executor: `rclpy.executors.MultiThreadedExecutor()`

![executors](https://docs.ros.org/en/foxy/_images/executors_basic_principle.png)

By invoking `spin()` of the Executor instance, the current thread starts querying
the `rcl` and middleware layers for incoming messages and other events and calls
the corresponding callback functions until the node shuts down. In order not to
counteract the `QoS` settings of the middleware, an incoming message is not stored
in a queue on the Client Library layer but kept in the middleware until it is
taken for processing by a callback function. (This is a crucial difference to
ROS 1.) A wait set is used to inform the Executor about available messages on
the middleware layer, with one binary flag per queue. The wait set is also used
to detect when timers expire.

### Executors types

![executors_types](https://docs.ros.org/en/foxy/_images/graphviz-c1160194dae16051e00be2abef23d0fce5e7c347.png)

The `Multi-Threaded Executor` creates a configurable number of threads to allow
for processing multiple messages or events in parallel.

The `Static Single-Threaded Executor` optimizes the runtime costs for scanning
the structure of a node in terms of subscriptions, timers, service servers,
action servers, etc. It performs this scan only once when the node is added,
while the other two executors regularly scan for such changes. Therefore, the
Static Single-Threaded Executor should be used only with nodes that create all
subscriptions, timers, etc. during initialization.

### Example

```python
import rclpy
from rclpy.node import Node
from rcply.executors import SingleThreadedExecutor

class MinimalPublisher(Node):

    def __init__(self):
        super().__init__('minimal_publisher')
        self.publisher_ = self.create_publisher(String, 'topic', 10)
        timer_period = 0.5  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)
        self.i = 0

    def timer_callback(self):
        msg = String()
        msg.data = 'Hello World: %d' % self.i
        self.publisher_.publish(msg)
        self.get_logger().info('Publishing: "%s"' % msg.data)
        self.i += 1


def main(args=None):
    rclpy.init(args=args)
    minimal_publisher = MinimalPublisher()
    executor = SingleThreadedExecutor()
    executor.add_node(minimal_publisher)
    try:
        executor.spin()
    finally:
        executor.shutdown()
        minimal_publisher.destroy_node()
        rclpy.shutdown()


if __name__ == '__main__':
    main()
```

## Transformations

### Transformations tree - prerequisites

Rotation representation - RPY angles:

- RPY -> roll, pitch, yaw (X, Y, Z)
- ZYX Euler angles
- Three values representation
- Drawback
    - Singular configurations (system lose one DOF) if two axes are aligned (Gimbal lock)
    - Non-unique representation (multiple angles can represent the same rotation)

Rotation representation - Quaternions:

- Four values representation
- No singular configurations
- Unique representation
- Drawback
    - Hard to interpret

`q = [x, y, z, w]`

Rotation representation - Rotation matrix:

- Nine values representation
- No singular configurations
- Unique representation
- Can be used to represent translation also
- Drawback
    - Hard to interpret

Transformation matrix that is used to perform a rotation in Euclidean space.

### `tf2`

`tf2` (transform library) is a library that provides support for managing
coordinate system in a robotic system.

- Provides support for handling relationship between different coordinate frames.
- Essential for most of robotic tasks such as sensor fusion, localization, etc.
- Enables easy conversion of data between different coordinate frames.
- Transformation = translation + rotation
- Coordinate system → frame
- Components: Dynamic Broadcaster, Static Broadcaster, Listener

#### `tf2` - Dynamic Broadcaster

- Broadcasts transformations between coordinate frames
- `tf2_ros::TransformBroadcaster`
- `tf2_ros::TransformBroadcaster::sendTransform()`
- Useful for scenarios where relationship between frames change over time

#### `tf2` - Static Broadcaster

- Defines transformations that never change
- `tf2_ros::StaticTransformBroadcaster`
- `tf2_ros::StaticTransformBroadcaster::sendTransform()`
- Useful for scenarios where relationship between frames never change
- Improves performance by eliminating the need to continuously broadcast
  transformations

#### `tf2` - Listener

- Listens to transformations between coordinate frames
- Essential for scenarios where relationship between frames change over time
- Enables adaptability in response to changes in robot environment
- Essential for tasks requiring knowledge of spatial relationships in real-time

## Debugging tools: rqt, view_frames

### `rqt_graph`

- Graphical tool for visualizing ROS computation graph
- Displays nodes and topics

### `view_frames`

- Graphical tool for visualizing `tf2` frames
- Displays frames and their relationships

