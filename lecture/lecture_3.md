# Lecture 3 - Introduction to ROS/ROS2

## Introduction to ROS

ROS is a set of libraries and tools that help build robot applications. From
drivers to state-of-the-art algorithms, and with powerful developer tools, ROS
has what you need for the robotic project.

Features:

- Open-source software
- Supported languages: C++, Python, C#, Rust
- Component-based approach

Currently the most popular framework for robotics (additionally the most
populate for the pas 10 years).

Motivation:

- Reuse of code
- Easy to integrate
- Easy to test
- Easy to debug
- Easy to maintain

### History

- 2007: ROS started as a research project at Stanford University
- 2009: ROS was released under the BSD license
- 2010: Willow Garage started to support ROS
- 2012: ROS was ported to OS X, Android, and ARM
- 2013: ROS was ported to Windows
- 2014: ROS2 was announced

### ROS vs ROS2

ROS2 is a new version of ROS that is a complete rewrite of ROS. It is not
compatible with ROS. ROS2 is a new framework that is designed to address the
limitations of ROS.

Limitations of ROS:

- **ROS is not real-time** -- only planned for non real-time applications
- **ROS is not secure** -- Can connect and see all the traffic with the content
  of the messages
- **ROS is not distributed** --  single point of failure - ROS Master

#### Differences

ROS:

- ROS core: ROS Master, rosout, parameter server, rosapi

ROS2:

- DDS (Data Distribution Service) - middleware for real-time and secure
  communication
- No central point in the system - no single point of failure
- Supports

#### Architecture

![ROS vs ROS2](./images/ros1_ros2.png)

### ROS-Industrial

...

### Application of ROS

- Mobile robots
- Manipulators
- Mobile manipulators
- Humanoid robots
- Autonomous cars
- Drones

ROS is not super lightweight, so it is not suitable for microcontrollers.

### Example robots

- PR2 (Personal Robot 2) - Willow Garage
  - Fully integrated with ROS
  - Features: autonomous navigation, manipulation, perception, grasping, etc.
  - Sensors: cameras, lidars, RGB-D, IMU,
- TurtleBot2 - Willow Garage
  - Fully integrated with ROS
  - Features: autonomous navigation
  - Sensors: encoders, bumpers, RGB-D camera
  - Low cost

### ROS2 Architercture

1. `rmw` - ROS Middleware Abstraction Interface - provides a common API for
   different DDS implementations
2. `rcl` - ROS Client Library - provides a common API for different languages
   (C, C++, Python, C#), do an abstraction of the rmw

## DDS

DDS (Data Distribution Service) is a middleware for real-time and secure
communication. Use the IDL (Interface Definition Language) to define the
messages. Provides dynamic discovery of the participants in the system. It is
not a protocol, it is a specification.

## Nodes in ROS2

Node is a independent programming unit. It is a process that performs
single activity. It can be a sensor, a controller, a driver, etc. Can be
written in different languages.

PICTURE DOCS.ROS.ORG

### Node - how to run?

```bash
ros2 run <package_name> <executable_name>
```

```bash
ros2 launch <package_name> <launch_file_name>
```

## Basic communication mechanism: topics

### Communication - topics

- Based on publish/subscribe pattern
- Message based
- Messages defined as `.msg` files
- Underlaying communication is based on DDS
- Communication frequency depends on the publisher
- Standard messages (in packages):
  - `std_msgs`
  - `sensor_msgs`
  - `geometry_msgs`

### Message definitions

- Pose.msg

    ```bash
    # A repesentation of pose in the free space
    Point position
    Quaternion orientation
    ```

- Point.msg

    ```bash
    # This contains the position of a point in free space
    float64 x
    float64 y
    float64 z
    ```

- Quaternion.msg

    ```bash
    # This represents an orientation in free space in quaternion form
    float64 x 0
    float64 y 0
    float64 z 0
    float64 w 0
    ```

### Subscriber - code example

```python
import rclpy
from rclpy.node import Node
from std_msgs.msg import String


class MinimalSubscriber(Node):
    def __init__(self):
        super().__init__('minimal_subscriber')
        self.subscription = self.create_subscription(
            String,
            'topic',
            self.listener_callback,
            10)
        self.subscription  # prevent unused variable warning

    def listener_callback(self, msg):
        self.get_logger().info('I heard: "%s"' % msg.data)


def main(args=None):
    rclpy.init(args=args)
    minimal_subscriber = MinimalSubscriber()
    rclpy.spin(minimal_subscriber)
    minimal_subscriber.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()

```

### Publisher - code example

```python
import rclpy
from rclpy.node import Node
from std_msgs.msg import String


class MinimalPublisher(Node):
    def __init__(self):
        super().__init__('minimal_publisher')
        self.publisher_ = self.create_publisher(String, 'topic', 10)
        timer_period = 0.5  # seconds
        self.timer = self.create_timer(timer_period, self.timer_callback)
        self.i = 0

    def timer_callback(self):
        msg = String()
        msg.data = 'Hello World: %d' % self.i
        self.publisher_.publish(msg)
        self.get_logger().info('Publishing: "%s"' % msg.data)
        self.i += 1

def main(args=None):
    rclpy.init(args=args)
    minimal_publisher = MinimalPublisher()
    rclpy.spin(minimal_publisher)
    minimal_publisher.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
```

## QoS (Quality of Service) in DDS

**QoS profiles - defines a set of policies:**

- History
  - Keep last - store only up to N message
  - Keep all - store all messages
- Depth - size of the queue
- Reliability
  - Best effort - no guarantee that the message will be delivered (may be lost if the network is not reliable)
  - Reliable - guarantee that the message will be delivered (try multiple times)
- Durability
  - Transient local - store messages only in the local queue until the receiver is available
  - Volatile - no attempt to store messages

### QoS settings - code example

```python
import rclpy
from std_msgs.msg import String


rclpy.init()
node = rclpy.create_node('minimal_publisher')

publisher_qos = rclpy.qos.QoSProfile(
    history=rclpy.qos.QoSHistoryPolicy.KEEP_LAST,
    depth=10,
    reliability=rclpy.qos.QoSReliabilityPolicy.RELIABLE,
    durability=rclpy.qos.QoSDurabilityPolicy.TRANSIENT_LOCAL
)

publisher = node.create_publisher(String, 'topic', publisher_qos)
```

## ROS Package

The ROS package is independent unit of building. It can contain:

- nodes,
- launch files,
- configuration files,
- message definitions,
- tools/scripts,
- tests.
