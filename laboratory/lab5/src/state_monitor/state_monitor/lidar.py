"""Lidar node."""

import rclpy

from state_monitor.sensor import SensorNode


def main(args=None):
    """Run lidar node."""
    rclpy.init(args=args)
    lidar_node = SensorNode("lidar")
    rclpy.spin(lidar_node)
    lidar_node.destroy_node()
    rclpy.shutdown()


if __name__ == "__main__":
    main()
