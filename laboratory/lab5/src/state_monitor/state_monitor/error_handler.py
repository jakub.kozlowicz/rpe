"""Error handler node."""

import functools

import rclpy
from rclpy.action import ActionClient
from rclpy.node import Node
from state_monitor_interfaces.action import RestartRequest
from state_monitor_interfaces.srv import ComponentError


class ErrorHandlerNode(Node):
    """Error handler node."""

    def __init__(self):
        """Initialize node."""
        super().__init__("error_handler")  # type: ignore

        self.component_error_service = self.create_service(
            ComponentError,
            "component_error",
            self.component_error_callback,
        )

        self.to_restart = set()
        self.in_restart = set()
        self.recovery_clients = {}
        self.recovery_futures = {}
        self.recovery_timer = self.create_timer(0.5, self.restart_components)

    def component_error_callback(self, request, response):
        """Handle component error request."""
        if request.component_name not in self.to_restart:
            self.get_logger().info(
                "Adding component %s to restart." % request.component_name,
            )
            self.to_restart.add(request.component_name)
            response.result = (
                f"Scheduled restart for component '{request.component_name}'."
            )
        elif request.component_name in self.in_restart:
            self.get_logger().info(
                "Component %s already in restart." % request.component_name,
            )
            response.result = "Already in restart."
        else:
            self.get_logger().info(
                "Component %s already scheduled for restart." % request.component_name,
            )
            response.result = "Already scheduled for restart."

        return response

    def restart_components(self):
        """Restart components that needs to be restarted."""
        if not self.to_restart or not self.to_restart.difference(self.in_restart):
            return

        for component in self.to_restart:
            self.get_logger().info("Restarting %s." % component)
            if component not in self.recovery_clients:
                self.get_logger().info("Creating recovery client for %s." % component)
                self.recovery_clients[component] = ActionClient(
                    self,
                    RestartRequest,
                    f"/{component}_recovery",
                )

                self.recovery_clients[component].wait_for_server()

            if component in self.in_restart:
                continue

            self.in_restart.add(component)
            goal = RestartRequest.Goal()
            goal.time = 1.0
            self.recovery_futures[component] = self.recovery_clients[
                component
            ].send_goal_async(
                goal,
                feedback_callback=functools.partial(
                    self.feedback_callback,
                    component=component,
                ),
            )
            self.recovery_futures[component].add_done_callback(
                functools.partial(
                    self.recovery_response_callback,
                    component=component,
                ),
            )

    def feedback_callback(self, feedback_msg, component):
        remaining_time = feedback_msg.feedback.remaining
        self.get_logger().info(
            "'%s' remains in recovery for %f seconds." % (component, remaining_time),
        )

    def recovery_response_callback(self, future, component):
        """Callback for recovery response."""
        goal_handle = future.result()
        if not goal_handle.accepted:
            self.get_logger().warning("Recovery action not accepted.")
            return

        self.get_logger().info("Recovery action accepted for %s." % component)

        _result_future = goal_handle.get_result_async()
        _result_future.add_done_callback(
            functools.partial(self.get_result_callback, component=component)
        )

    def get_result_callback(self, future, component):
        """Callback for recovery result."""
        result = future.result().result.success
        self.to_restart.remove(component)
        self.in_restart.remove(component)
        self.get_logger().info("Component %s restarted. (%s)" % (component, result))


def main(args=None):
    """Run error handler node."""
    rclpy.init(args=args)
    error_handler_node = ErrorHandlerNode()
    rclpy.spin(error_handler_node)
    error_handler_node.destroy_node()
    rclpy.shutdown()


if __name__ == "__main__":
    main()
