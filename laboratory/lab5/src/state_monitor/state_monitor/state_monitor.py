"""State monitor node."""

import rclpy
from rclpy.node import Node, Parameter
from state_monitor_interfaces.srv import ComponentError
from std_msgs.msg import Bool


class StateMonitorNode(Node):
    """Error handler node."""

    def __init__(self):
        """Initialize node."""
        super().__init__("state_monitor")  # type: ignore

        self.declare_parameters(
            namespace="",
            parameters=[
                ("sensors", Parameter.Type.STRING_ARRAY),
                ("state_check_frequency", 1.0),  # type: ignore
            ],
        )

        self.sensors = (
            self.get_parameter("sensors").get_parameter_value().string_array_value
        )
        self.get_logger().info("Monitoring sensors: %s" % self.sensors)
        self.state = {sensor: True for sensor in self.sensors}
        self.state_subscribers = {}
        for sensor in self.sensors:
            self.state_subscribers[sensor] = self.create_subscription(
                Bool,
                f"/{sensor}_state",
                self.state_callback(sensor),
                10,
            )

        self.state_check_frequency = (
            self.get_parameter("state_check_frequency")
            .get_parameter_value()
            .double_value
        )
        self.state_publisher = self.create_publisher(Bool, "/robot_state", 10)
        self.state_timer = self.create_timer(
            self.state_check_frequency,
            self.publish_state,
        )

        self.component_error_client = self.create_client(
            ComponentError,
            "component_error",
        )
        while not self.component_error_client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info(
                "Service for component error not available, waiting again...",
            )

        self.component_error_futures = []
        self.future_processing_timer = self.create_timer(
            0.2,
            self.process_component_error_futures,
        )

    def publish_state(self):
        """Publish state."""
        msg = Bool()
        msg.data = all(self.state.values())
        self.state_publisher.publish(msg)

    def state_callback(self, sensor):
        """Return state callback."""

        def callback(msg):
            self.state[sensor] = msg.data
            if not msg.data:
                self.get_logger().error(
                    "'%s' is not working. Sending error..." % sensor,
                )
                request = ComponentError.Request()
                request.component_name = sensor
                future = self.component_error_client.call_async(
                    request,
                )
                self.component_error_futures.append(future)

        return callback

    def process_component_error_futures(self):
        if not self.component_error_futures:
            return

        for future in self.component_error_futures:
            if future.done():
                try:
                    response = future.result()
                    self.get_logger().debug(
                        "Component error result: %s" % response.result,
                    )
                except Exception as e:
                    self.get_logger().error("Service call failed %r" % (e,))

        self.component_error_futures = []


def main(args=None):
    """Run state monitor node."""
    rclpy.init(args=args)
    state_monitor_node = StateMonitorNode()
    rclpy.spin(state_monitor_node)
    state_monitor_node.destroy_node()
    rclpy.shutdown()


if __name__ == "__main__":
    main()
