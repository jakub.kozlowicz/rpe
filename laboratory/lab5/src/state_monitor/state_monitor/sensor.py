"""Sensor node."""

import random
import time

import rclpy
from rclpy.action import ActionServer
from rclpy.node import Node
from state_monitor_interfaces.action import RestartRequest
from std_msgs.msg import Bool


class SensorNode(Node):
    """Sensor node."""

    def __init__(self, sensor_type: str):
        """Initialize node."""
        super().__init__(sensor_type)  # type: ignore

        self.declare_parameters(
            namespace="",
            parameters=[
                ("recovery_time", 5.0),  # type: ignore
                ("state_frequency", 1.0),
            ],
        )

        self.recovery_time = (
            self.get_parameter("recovery_time").get_parameter_value().double_value
        )

        self.state_frequency = (
            self.get_parameter("state_frequency").get_parameter_value().double_value
        )

        self.in_recovery = False

        self.state_publisher = self.create_publisher(Bool, f"/{sensor_type}_state", 10)
        self.state_timer = self.create_timer(self.state_frequency, self.publish_state)

        self.restart_server = ActionServer(
            self,
            RestartRequest,
            f"/{sensor_type}_recovery",
            self.execute_recovery,
        )

    def publish_state(self):
        """Publish state of the sensor."""
        msg = Bool()
        state = self._check_state()
        if not state:
            self.get_logger().warn("Sensor in error state")
        msg.data = state
        self.state_publisher.publish(msg)

    def execute_recovery(self, goal_handle):
        """Execute recovery."""
        self.get_logger().info("Executing recovery")

        if not self.recovery_time:
            self.get_logger().warning(
                "Recovery time not set. Falling back to default [5s].",
            )
            self.recovery_time = 5.0

        feedback_msg = RestartRequest.Feedback()
        remaining_time = self.recovery_time
        self.in_recovery = True
        while remaining_time > 0.0 and not goal_handle.is_cancel_requested:
            feedback_msg.remaining = remaining_time
            goal_handle.publish_feedback(feedback_msg)
            remaining_time -= 1.0
            self.get_logger().debug("Remaining Time: %.2f seconds" % remaining_time)
            time.sleep(1)

        if goal_handle.is_cancel_requested:
            self.in_recovery = False
            goal_handle.canceled()
            self.get_logger().info("Recovery canceled")
            return RestartRequest.Result()

        self.in_recovery = False
        goal_handle.succeed()
        self.get_logger().info("Recovery completed")
        result = RestartRequest.Result()
        result.success = True
        return result

    def _check_state(self) -> bool:
        """Check state of the sensor."""
        if self.in_recovery:
            return False

        state = random.uniform(0.0, 1.0)
        if state <= 0.15:
            return False

        return True
