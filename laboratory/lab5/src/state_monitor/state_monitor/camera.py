"""Camera node."""

import rclpy

from state_monitor.sensor import SensorNode


def main(args=None):
    """Run camera node."""
    rclpy.init(args=args)
    camera_node = SensorNode("camera")
    rclpy.spin(camera_node)
    camera_node.destroy_node()
    rclpy.shutdown()


if __name__ == "__main__":
    main()
