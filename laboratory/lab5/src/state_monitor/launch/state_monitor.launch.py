"""Launcher for the state monitor package."""

import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node


def get_config_file(package_name, file_name):
    """Get path to config file."""
    config_path = os.path.join(
        get_package_share_directory(package_name),
        "config",
        file_name,
    )
    return config_path


def generate_launch_description():
    camera_config = get_config_file("state_monitor", "camera.yaml")
    lidar_config = get_config_file("state_monitor", "lidar.yaml")
    error_handler_config = get_config_file("state_monitor", "error_handler.yaml")
    state_monitor_config = get_config_file("state_monitor", "state_monitor.yaml")

    return LaunchDescription(
        [
            Node(
                package="state_monitor",
                executable="camera",
                name="camera",
                parameters=[camera_config],
            ),
            Node(
                package="state_monitor",
                executable="lidar",
                name="lidar",
                parameters=[lidar_config],
            ),
            Node(
                package="state_monitor",
                executable="error_handler",
                name="error_handler",
                parameters=[error_handler_config],
            ),
            Node(
                package="state_monitor",
                executable="state_monitor",
                name="state_monitor",
                parameters=[state_monitor_config],
            ),
        ]
    )
