import glob
import os
from setuptools import setup

package_name = "state_monitor"

setup(
    name=package_name,
    version="0.0.0",
    packages=[package_name],
    data_files=[
        ("share/ament_index/resource_index/packages", ["resource/" + package_name]),
        ("share/" + package_name, ["package.xml"]),
        # Include all launch files.
        (
            os.path.join("share", package_name, "launch"),
            glob.glob(os.path.join("launch", "*.launch.py")),
        ),
        (os.path.join("share", package_name, "config"), glob.glob("config/*.yaml")),
    ],
    install_requires=["setuptools"],
    zip_safe=True,
    maintainer="Jakub Kozlowicz",
    maintainer_email="252865@student.pwr.edu.pl",
    description="State monitor package",
    license="MIT",
    tests_require=["pytest"],
    entry_points={
        "console_scripts": [
            "lidar = state_monitor.lidar:main",
            "camera = state_monitor.camera:main",
            "state_monitor = state_monitor.state_monitor:main",
            "error_handler = state_monitor.error_handler:main",
        ],
    },
)
