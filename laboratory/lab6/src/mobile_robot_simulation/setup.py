import glob
import os

from setuptools import find_packages, setup

package_name = "mobile_robot_simulation"

setup(
    name=package_name,
    version="0.0.0",
    packages=find_packages(exclude=["test"]),
    data_files=[
        ("share/ament_index/resource_index/packages", ["resource/" + package_name]),
        ("share/" + package_name, ["package.xml"]),
        (
            os.path.join("share", package_name, "launch"),
            glob.glob(os.path.join("launch", "*.launch.py")),
        ),
        (os.path.join("share", package_name, "config"), glob.glob("config/*.yaml")),
    ],
    install_requires=["setuptools"],
    zip_safe=True,
    maintainer="Jakub Kozłowicz",
    maintainer_email="252865@student.pwr.edu.pl",
    description="Simulation of the mobile robot in ROS2",
    license="MIT",
    tests_require=["pytest"],
    entry_points={
        "console_scripts": [
            "robot = mobile_robot_simulation.robot.node:main",
            "controller = mobile_robot_simulation.controller.node:main",
        ],
    },
)
