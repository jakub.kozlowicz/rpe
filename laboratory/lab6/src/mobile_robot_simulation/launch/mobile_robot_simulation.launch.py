"""Launcher for the mobile robot simulation package."""

import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node


def get_config_file_path(package_name: str, file_name: str) -> str:
    """Get path to config file."""
    config_path = os.path.join(
        get_package_share_directory(package_name),
        "config",
        file_name,
    )
    return config_path


def generate_launch_description() -> LaunchDescription:
    simulation_config = get_config_file_path("mobile_robot_simulation", "config.yaml")
    rviz_config = get_config_file_path("mobile_robot_simulation", "rviz_config.yaml")

    return LaunchDescription(
        [
            Node(
                package="mobile_robot_simulation",
                executable="controller",
                name="controller",
                parameters=[simulation_config],
            ),
            Node(
                package="mobile_robot_simulation",
                executable="robot",
                name="robot",
                parameters=[simulation_config],
            ),
            Node(
                package="rviz2",
                executable="rviz2",
                name="rviz2",
                arguments=["-d", rviz_config],
            ),
        ]
    )
