"""Controller node."""


import rclpy
from geometry_msgs.msg import Twist
from rclpy.node import Node

from .trajectory_generator import TrajectoryGenerator


class ControllerNode(Node):
    """Controller node."""

    def __init__(self) -> None:
        """Initialize node."""
        super().__init__("controller")  # type: ignore

        self.get_logger().debug("Controller node started")

        self.declare_parameters(
            namespace="",
            parameters=[
                ("control_publish_frequency", 10.0),  # type: ignore
                ("square_length", 4.0),
                ("moving_speed", 0.7),
                ("turning_speed", 0.2),
            ],
        )

        self.control_publish_frequency = (
            self.get_parameter("control_publish_frequency")
            .get_parameter_value()
            .double_value
        )
        self.square_length = (
            self.get_parameter("square_length").get_parameter_value().double_value
        )
        self.moving_speed = (
            self.get_parameter("moving_speed").get_parameter_value().double_value
        )
        self.turning_speed = (
            self.get_parameter("turning_speed").get_parameter_value().double_value
        )

        self.publisher = self.create_publisher(Twist, "/control", 10)
        self.timer = self.create_timer(
            1 / self.control_publish_frequency,
            self.publish_control,
        )

        self.trajectory_generator = TrajectoryGenerator(
            frequency=self.control_publish_frequency,
            square_length=self.square_length,
            moving_speed=self.moving_speed,
            turning_speed=self.turning_speed,
        )

        self.get_logger().info("Controller node initialized")

    def publish_control(self) -> None:
        """Publish Twist message."""

        msg = self.trajectory_generator.get_value()
        self.publisher.publish(msg)

        self.get_logger().debug(f"Published Twist message: {msg}")


def main(args=None):
    """Run controller node."""
    rclpy.init(args=args)
    node = ControllerNode()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()


if __name__ == "__main__":
    main()
