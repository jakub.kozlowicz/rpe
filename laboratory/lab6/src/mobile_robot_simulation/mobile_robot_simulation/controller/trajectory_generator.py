"""Generates a trajectory for the robot to follow."""

import dataclasses
import enum
import math
import typing as t

from geometry_msgs.msg import Twist, Vector3


@dataclasses.dataclass
class Velocity:
    """Velocity of the robot."""

    x: float = 0.0
    y: float = 0.0
    z: float = 0.0

    def set_value(
        self,
        x: t.Optional[float] = None,
        y: t.Optional[float] = None,
        z: t.Optional[float] = None,
    ) -> None:
        """Set the value of the velocity."""
        if x is not None:
            self.x = x

        if y is not None:
            self.y = y

        if z is not None:
            self.z = z

    def to_dict(self) -> t.Dict[str, float]:
        """Convert to dictionary."""
        return dataclasses.asdict(self)


class State(enum.Enum):
    """State of the robot."""

    MOVE_STRAIGHT = enum.auto()
    TURN = enum.auto()


class TrajectoryGenerator:
    """Generates a trajectory for the robot to follow."""

    def __init__(
        self,
        frequency: float,
        square_length: float,
        moving_speed: float = 0.5,
        turning_speed: float = 0.5,
    ) -> None:
        """Initialize the trajectory generator."""
        self._frequency = frequency
        self._square_length = square_length
        self._moving_speed = moving_speed
        self._turning_speed = turning_speed

        self._call_counter = 0.0
        self._state = State.MOVE_STRAIGHT

        self._square_calls = self.calculate_square_calls()
        self._turn_calls = self.calculate_turn_calls() - 1

        self._linear_velocity = Velocity(x=0.5)
        self._angular_velocity = Velocity(z=0.0)

    def get_value(self) -> Twist:
        """Get the current value of the trajectory."""

        if self._state == State.MOVE_STRAIGHT:
            self._linear_velocity.set_value(x=self._moving_speed)
            self._angular_velocity.set_value(z=0.0)
            self._call_counter += 1

            if self._call_counter >= self._square_calls:
                self._state = State.TURN
                self._call_counter = 0

        elif self._state == State.TURN:
            self._linear_velocity = Velocity(x=0.0)
            self._angular_velocity = Velocity(z=self._turning_speed)
            self._call_counter += 1

            if self._call_counter >= self._turn_calls:
                self._state = State.MOVE_STRAIGHT
                self._call_counter = 0

        twist = Twist()
        twist.linear = Vector3(**self._linear_velocity.to_dict())
        twist.angular = Vector3(**self._angular_velocity.to_dict())
        return twist

    def calculate_turn_calls(self) -> float:
        """Calculate the number of calls for a 90-degree turn."""
        # Angle = angular speed * time
        # Time = angle / angular speed
        # Number of calls = time * frequency
        turn_angle = math.pi / 2  # 90 degrees in radians
        turn_time = turn_angle / self._turning_speed
        return turn_time * self._frequency

    def calculate_square_calls(self) -> float:
        """Calculate the number of calls for a square."""
        # Distance = speed * time
        # Time = distance / speed
        # Number of calls = time * frequency
        square_time = self._square_length / self._moving_speed
        return square_time * self._frequency
