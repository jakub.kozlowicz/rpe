"""Robot position."""


import dataclasses
import typing as t

import numpy as np
from geometry_msgs.msg import Point, Pose, Quaternion, Transform, Vector3


@dataclasses.dataclass
class RobotPosition:
    """Represents robot position as x, y and theta.

    The Z coordinate is not used in this case.

    :param x: X coordinate.
    :type x: float
    :param y: Y coordinate.
    :type y: float
    :param z: Z coordinate.
    :type z: float
    :param theta: Theta coordinate.
    :type theta: float

    :return: Robot position.
    :rtype: RobotPosition
    """

    x: float = 0.0
    y: float = 0.0
    z: float = 0.0
    theta: float = 0.0

    def get_values(self) -> t.Tuple[float, float, float]:
        """Get values of current robot position.

        :return: Current robot position as x, y and theta.
        :rtype: tuple[float, float, float]
        """

        return self.x, self.y, self.theta

    def update(
        self,
        dt: float,
        linear_velocity: float,
        angular_velocity: float,
    ) -> None:
        """Update robot position.

        :param dt: Time delta.
        :type dt: float
        :param linear_velocity: Linear velocity.
        :type linear_velocity: float
        :param angular_velocity: Angular velocity.
        :type angular_velocity: float
        """

        self.x += dt * linear_velocity * np.cos(self.theta)
        self.y += dt * linear_velocity * np.sin(self.theta)
        self.theta += dt * angular_velocity

    def to_pose(self) -> Pose:
        """Convert robot position to ROS message.

        :return: Robot position as ROS message.
        :rtype: Pose
        """

        position = Point(x=self.x, y=self.y, z=self.z)
        orientation = Quaternion(
            x=0.0,
            y=0.0,
            z=np.sin(self.theta / 2),
            w=np.cos(self.theta / 2),
        )

        return Pose(position=position, orientation=orientation)

    def to_transform(self) -> Transform:
        """Convert robot position to ROS message.

        :return: Robot position as ROS message.
        :rtype: Transform
        """

        translation = Vector3(x=self.x, y=self.y, z=self.z)
        rotation = Quaternion(
            x=0.0,
            y=0.0,
            z=np.sin(self.theta / 2),
            w=np.cos(self.theta / 2),
        )

        return Transform(translation=translation, rotation=rotation)
