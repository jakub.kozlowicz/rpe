"""Robot control."""


import dataclasses
import datetime
import typing as t


@dataclasses.dataclass
class RobotControl:
    """Represents robot control.

    :param linear_velocity: Linear velocity of the control.
    :type linear_velocity: float
    :param angular_velocity: Angular velocity of the control.
    :type angular_velocity: float
    :param control_ttl: Control time to live. (default: 1 second)
    :type control_ttl: datetime.timedelta
    :param _control_updated_at: Time when control was updated. (default: now)
    :type _control_updated_at: datetime.datetime
    :return: Robot control.
    :rtype: RobotControl
    """

    linear_velocity: float = 0.0
    angular_velocity: float = 0.0

    control_ttl: datetime.timedelta = datetime.timedelta(seconds=1)

    _control_updated_at: datetime.datetime = dataclasses.field(
        default_factory=lambda: datetime.datetime.now(tz=datetime.timezone.utc),
        init=False,
    )

    def get_values(self) -> t.Tuple[float, float]:
        """Return linear and angular velocity.

        :return: Linear and angular velocity.
        :rtype: t.Tuple[float, float]
        """

        now = datetime.datetime.now(tz=datetime.timezone.utc)
        if now - self._control_updated_at > self.control_ttl:
            return 0.0, 0.0

        return self.linear_velocity, self.angular_velocity

    def update(self, linear_velocity: float, angular_velocity: float) -> None:
        """Update control values.

        :param linear_velocity: Linear velocity.
        :type linear_velocity: float
        :param angular_velocity: Angular velocity.
        :type angular_velocity: float
        """

        now = datetime.datetime.now(tz=datetime.timezone.utc)

        self.linear_velocity = linear_velocity
        self.angular_velocity = angular_velocity
        self._control_updated_at = now
