"""Robot node."""


import datetime

import rclpy
from geometry_msgs.msg import PoseStamped, Twist
from rclpy.node import Node
from tf2_ros import TransformBroadcaster, TransformStamped

from .control import RobotControl
from .position import RobotPosition


class RobotNode(Node):
    """Robot node."""

    def __init__(self) -> None:
        """Initialize node."""
        super().__init__("robot")  # type: ignore

        self.get_logger().debug("Robot node started")

        self.declare_parameters(
            namespace="",
            parameters=[
                ("pose_publish_frequency", 10.0),  # type: ignore
            ],
        )

        self.pose_publish_frequency = (
            self.get_parameter("pose_publish_frequency")
            .get_parameter_value()
            .double_value
        )

        self.robot_control = RobotControl(
            control_ttl=datetime.timedelta(seconds=(1 / self.pose_publish_frequency)),
        )
        self.robot_position = RobotPosition()
        self.time = 0.0

        self.control_subscription = self.create_subscription(
            Twist,
            "/control",
            self.control_callback,
            10,
        )

        self.tf_broadcaster = TransformBroadcaster(self)

        self.pose_publisher = self.create_publisher(PoseStamped, "/robot_pose", 10)

        self.pose_timer = self.create_timer(
            1 / self.pose_publish_frequency,
            self.pose_callback,
        )

        self.get_logger().info("Robot node initialized")

    def control_callback(self, msg: Twist) -> None:
        """Callback for the control message.

        :param msg: Message with control values.
        :type msg: Twist
        """
        self.get_logger().debug(f"Received Twist message: {msg}")

        self.robot_control.update(msg.linear.x, msg.angular.z)

    def pose_callback(self) -> None:
        """Publish PoseStamped message."""
        dt = 1.0 / self.pose_publish_frequency
        self.robot_position.update(dt, *self.robot_control.get_values())

        pose = PoseStamped()
        pose.header.frame_id = "world"
        pose.header.stamp = self.get_clock().now().to_msg()
        pose.pose = self.robot_position.to_pose()

        self.pose_publisher.publish(pose)

        transform_msg = TransformStamped()
        transform_msg.header.stamp = self.get_clock().now().to_msg()
        transform_msg.header.frame_id = "world"
        transform_msg.child_frame_id = "base_link"
        transform_msg.transform = self.robot_position.to_transform()

        self.tf_broadcaster.sendTransform(transform_msg)

        self.get_logger().debug(f"Published PoseStamped message: {pose}")


def main(args=None):
    """Run robot node."""
    rclpy.init(args=args)
    node = RobotNode()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()


if __name__ == "__main__":
    main()
