"""Graph optimization for SE3 pose graph."""


import logging
import typing as t

import g2o
import matplotlib.pyplot as plt
import numpy as np

logger = logging.getLogger("optimizer_se3")


class PoseGraphOptimization(g2o.SparseOptimizer):
    """Graph optimization for SE3 pose graph."""

    def __init__(self, verbose: bool = False) -> None:
        """Initialize the graph optimization."""

        super().__init__()
        solver = g2o.BlockSolverSE3(g2o.LinearSolverEigenSE3())
        super().set_algorithm(g2o.OptimizationAlgorithmLevenberg(solver))
        super().set_verbose(verbose)

    def optimize(self, max_iterations: int = 20) -> None:
        """Optimize the graph."""

        super().initialize_optimization()
        super().optimize(max_iterations)

    def add_vertex(
        self,
        id: int,
        pose: tuple[float, ...] | g2o.Isometry3d,
        fixed: bool = False,
    ) -> None:
        """Add a vertex to the graph."""

        if isinstance(pose, tuple):
            quaternion = np.array(pose[3:])
            translation = np.array(pose[:3])
            rotation = g2o.Quaternion(
                quaternion[3],
                quaternion[0],
                quaternion[1],
                quaternion[2],
            )
            pose = g2o.Isometry3d(rotation, translation)

        v_se3 = g2o.VertexSE3()
        v_se3.set_id(id)
        v_se3.set_estimate(pose)
        v_se3.set_fixed(fixed)
        super().add_vertex(v_se3)

    def add_edge(
        self,
        vertices: t.Iterable[int | g2o.VertexSE3],
        measurement: tuple[float, ...] | g2o.Isometry3d,
        information: np.ndarray = np.identity(6),
        robust_kernel: t.Optional[g2o.BaseRobustKernel] = None,
    ) -> None:
        """Add an edge to the graph."""

        edge = g2o.EdgeSE3()
        for i, v in enumerate(vertices):
            if isinstance(v, int):
                v = self.vertex(v)
            edge.set_vertex(i, v)

        if isinstance(measurement, tuple):
            quaternion = np.array(measurement[3:])
            translation = np.array(measurement[:3])
            rotation = g2o.Quaternion(
                quaternion[3],
                quaternion[0],
                quaternion[1],
                quaternion[2],
            )
            measurement = g2o.Isometry3d(rotation, translation)

        edge.set_measurement(measurement)
        edge.set_information(information)

        if robust_kernel is not None:
            edge.set_robust_kernel(robust_kernel)

        super().add_edge(edge)

    def get_pose(self, id: int) -> g2o.Isometry3d:
        """Get the pose of a vertex."""
        return self.vertex(id).estimate()


def visualize_poses(
    optimizer: PoseGraphOptimization,
    vertex_id_to_name: dict[int, str],
) -> None:
    """Visualize the poses in 3D."""
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.grid()

    for vertex_id, vertex_name in vertex_id_to_name.items():
        pose = optimizer.get_pose(vertex_id)
        translation = pose.translation()

        ax.scatter(translation[0], translation[1], translation[2], label=vertex_name)

    ax.set_xlabel("X-axis")
    ax.set_ylabel("Y-axis")
    ax.set_zlabel("Z-axis")
    ax.legend()
    plt.show()


def main(verbose: bool, max_iterations: int) -> int:
    """Execute the main routine for SE3."""
    optimizer = PoseGraphOptimization(verbose)

    vertex_id_to_name = {
        0: "x0",
        1: "x1",
        2: "x2",
        3: "x3",
        4: "f1",
        5: "f2",
        6: "f3",
    }

    vertices_relations = {
        (0, 1): (2.1, 0, 0, 0, 0, 0, 0),
        (1, 2): (1.9, 0, 0, 0, 0, 0, 0),
        (2, 3): (0.7, 0, 0, 0, 0, 0, 0),
        (0, 4): (0.5, 1.0, 1.0, 0, 0, 0, 0),
        (1, 4): (-1.5, 1.0, 2.0, 0, 0, 0, 0),
        (1, 6): (1.0, -1.0, 3.0, 0, 0, 0, 0),
        (2, 6): (-1.0, -1.0, 4.0, 0, 0, 0, 0),
        (2, 5): (0, 0, 5.0, 0, 0, 0, 0),
        (3, 5): (0, 0, 6.0, 0, 0, 0, 0),
    }

    # Add vertices
    for vertex_id in vertex_id_to_name.keys():
        optimizer.add_vertex(
            id=vertex_id,
            pose=(0, 0, 0, 0, 0, 0, 0),
            fixed=vertex_id == 0,
        )

    # Add edges
    for vertices, relation in vertices_relations.items():
        optimizer.add_edge(vertices, relation)

    optimizer.optimize(max_iterations)

    for vertex_id, vertex_name in vertex_id_to_name.items():
        pose = optimizer.get_pose(vertex_id)
        print(f"Vertex {vertex_name}: {pose.matrix()}")

    visualize_poses(optimizer, vertex_id_to_name)

    return 0
