"""Optimizer for the pose graph optimization."""


import argparse
import logging

from optimizer_se2 import main as main_se2
from optimizer_se3 import main as main_se3

logger = logging.getLogger("optimizer")


def main(args: list[str]) -> int:
    """Execute the main routine."""
    parser = argparse.ArgumentParser(description="Optimizer for the pose graph.")
    parser.add_argument(
        "type",
        type=str,
        choices=["se2", "se3"],
        help="Type of pose graph.",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        type=bool,
        default=False,
        help="Verbose output.",
    )
    parser.add_argument(
        "-n",
        "--max-iterations",
        type=int,
        default=30,
        help="Number of maximum iterations.",
    )

    parsed_args = parser.parse_args(args)

    logger_level = logging.DEBUG if parsed_args.verbose else logging.INFO

    logging.basicConfig(
        level=logger_level,
        format="%(asctime)s - [%(levelname)s] - %(name)s - %(message)s",
    )

    if parsed_args.type == "se2":
        return main_se2(parsed_args.verbose, parsed_args.max_iterations)

    elif parsed_args.type == "se3":
        return main_se3(parsed_args.verbose, parsed_args.max_iterations)

    else:
        raise NotImplementedError(f"Unknown pose graph type: {parsed_args.type}")


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv[1:]))
