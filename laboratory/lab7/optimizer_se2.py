"""Graph optimization for SE2 pose graph."""


import logging
import typing as t

import g2o
import matplotlib.pyplot as plt
import numpy as np

logger = logging.getLogger("optimizer_se2")


class PoseGraphOptimization(g2o.SparseOptimizer):
    """Graph optimization for SE2 pose graph."""

    def __init__(self, verbose: bool = False) -> None:
        """Initialize the graph optimization."""

        super().__init__()
        solver = g2o.BlockSolverSE2(g2o.LinearSolverEigenSE2())
        super().set_algorithm(g2o.OptimizationAlgorithmLevenberg(solver))
        super().set_verbose(verbose)

    def optimize(self, max_iterations: int = 20) -> None:
        """Optimize the graph."""

        super().initialize_optimization()
        super().optimize(max_iterations)

    def add_vertex(
        self,
        id: int,
        pose: tuple[float, ...] | g2o.SE2,
        fixed: bool = False,
    ) -> None:
        """Add a vertex to the graph."""

        if isinstance(pose, tuple):
            pose = g2o.SE2(*pose)

        v_se2 = g2o.VertexSE2()
        v_se2.set_id(id)
        v_se2.set_estimate(pose)
        v_se2.set_fixed(fixed)
        super().add_vertex(v_se2)

    def add_edge(
        self,
        vertices: t.Iterable[int | g2o.VertexSE2],
        measurement: tuple[float, ...] | g2o.SE2,
        information: np.ndarray = np.identity(3),
        robust_kernel: t.Optional[g2o.BaseRobustKernel] = None,
    ) -> None:
        """Add an edge to the graph."""

        edge = g2o.EdgeSE2()
        for i, v in enumerate(vertices):
            if isinstance(v, int):
                v = self.vertex(v)
            edge.set_vertex(i, v)

        if isinstance(measurement, tuple):
            measurement = g2o.SE2(*measurement)

        edge.set_measurement(measurement)
        edge.set_information(information)

        if robust_kernel is not None:
            edge.set_robust_kernel(robust_kernel)

        super().add_edge(edge)

    def get_pose(self, id: int) -> g2o.SE2:
        """Get the pose of a vertex."""
        return self.vertex(id).estimate()


def visualize_poses(
    optimizer: PoseGraphOptimization,
    vertex_id_to_name: dict[int, str],
) -> None:
    """Visualize the poses in the 2D."""
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.grid()

    for vertex_id, vertex_name in vertex_id_to_name.items():
        pose = optimizer.get_pose(vertex_id)
        translation = pose.translation()

        ax.scatter(translation[0], translation[1], label=vertex_name)

    ax.set_xlabel("X-axis")
    ax.set_ylabel("Y-axis")
    ax.legend()
    plt.show()


def main(verbose: bool, max_iterations: int) -> int:
    """Execute the main routine for SE2."""
    optimizer = PoseGraphOptimization(verbose)

    vertex_id_to_name = {
        0: "x0",
        1: "x1",
        2: "x2",
        3: "f1",
        4: "f2",
    }

    vertices_relations = {
        (0, 1): (2.1, 0, 0),
        (1, 2): (1.9, 0, 0),
        (0, 3): (0.5, 1.0, 0),
        (1, 3): (-1.5, 1.0, 0),
        (1, 4): (1.0, -1.0, 0),
        (2, 4): (-1.0, -1.0, 0),
    }

    # Add vertices
    for vertex_id in vertex_id_to_name.keys():
        optimizer.add_vertex(
            id=vertex_id,
            pose=(0, 0, 0),
            fixed=vertex_id == 0,
        )

    # Add edges
    for vertices, relation in vertices_relations.items():
        optimizer.add_edge(vertices, relation)

    optimizer.optimize(max_iterations)

    for vertex_id, vertex_name in vertex_id_to_name.items():
        pose = optimizer.get_pose(vertex_id)
        print(f"Vertex {vertex_name}: {pose.to_vector()}")

    visualize_poses(optimizer, vertex_id_to_name)

    return 0
