"""Server for receiving requests."""

import zmq

from pose_pb2 import Pose


def main(args: list[str]) -> int:
    """Main entrypoint for the server."""
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind("tcp://*:5555")

    while True:
        try:
            message = socket.recv()
            pose = Pose()
            pose.ParseFromString(message)
            print(f"Received request: {pose}")
            socket.send(b"Received")
        except KeyboardInterrupt:
            print("Exiting...")
            break

    socket.close()
    context.term()

    return 0


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
