"""Client for sending requests."""

import random
import time

import zmq

from pose_pb2 import Pose


def main(args: list[str]) -> int:
    """Main client entrypoint."""
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.connect("tcp://localhost:5555")

    while True:
        try:
            pose = Pose()
            pose.position.x = random.random()
            pose.position.y = random.random()
            pose.position.z = random.random()
            pose.orientation.x = random.random()
            pose.orientation.y = random.random()
            pose.orientation.z = random.random()
            pose.orientation.w = random.random()
            print(f"Sending request: {pose}")
            message = pose.SerializeToString()
            socket.send(message)
            response = socket.recv()
            print(f"Received response: {response.decode('utf-8')}")
            time.sleep(5)
        except KeyboardInterrupt:
            print("Exiting...")
            break

    socket.close()
    context.term()

    return 0


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
