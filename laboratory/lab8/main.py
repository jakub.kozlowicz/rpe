"""CasADi optimization example."""


import casadi as ca


def main(args: list[str]) -> int:
    """Main function."""

    # Symbolic variables
    x = ca.MX.sym("x")
    y = ca.MX.sym("y")
    z = ca.MX.sym("z")

    # Parameters
    p1, p2 = 5.0, 1.0

    # Objective function
    f = 3 * x**4 + y**3 + z**2 + 2 * x * y + 4 * z

    # Constants for variables
    # x, y, z >= 0
    lbx = ca.vertcat(0.0, 0.0, 0.0)
    ubx = ca.vertcat(ca.inf, ca.inf, ca.inf)

    # Define constraints
    # g1 >= 0, g2 >= 0
    # g1 <= 0, g2 <= 0
    # so that g1 = 0 and g2 = 0
    g1 = 8 * x + 4 * y + 2 * z - p1
    g2 = p2 * x + y - z - 1
    lbg = ca.vertcat(0.0, 0.0)
    ubg = ca.vertcat(0.0, 0.0)

    # Set initial guess
    x0 = ca.vertcat(1.0, 1.0, 1.0)

    solver = ca.nlpsol(
        # Name of the solver
        "solver",
        # Solver type
        "ipopt",
        # Optimization problem
        {
            "x": ca.vertcat(x, y, z),
            "f": f,
            "g": ca.vertcat(g1, g2),
        },
        # Solver options
        {
            "ipopt.print_level": 0,
            "print_time": 0,
        },
    )

    # Solve the optimization problem
    result = solver(x0=x0, lbx=lbx, ubx=ubx, lbg=lbg, ubg=ubg)
    optimal_solution = result["x"]

    # Print the optimal solution
    print("Optimal solution:")
    print(
        f"x: {optimal_solution[0]}, "
        f"y: {optimal_solution[1]}, "
        f"z: {optimal_solution[2]}"
    )

    return 0


if __name__ == "__main__":
    import sys

    sys.exit(main(sys.argv))
