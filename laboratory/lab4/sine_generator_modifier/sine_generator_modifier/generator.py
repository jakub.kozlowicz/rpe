"""Node that generates sine wave."""

import numpy as np
import rclpy
from rclpy.node import Node
from rclpy.parameter import Parameter
from std_msgs.msg import Float64


class Generator(Node):
    """Generator node that generates the sine wave."""

    def __init__(self) -> None:
        """Initialize class."""
        super().__init__("sine_wave_generator")
        self.declare_parameters(
            namespace="",
            parameters=[
                ("amplitude", Parameter.Type.DOUBLE),
                ("frequency", Parameter.Type.DOUBLE),
            ],
        )
        self.topic = "/signal"
        self._publisher = self.create_publisher(Float64, self.topic, 10)
        self.timer_period = 0.0005
        self.timer = self.create_timer(self.timer_period, self.timer_callback)
        self.index = 0

    def timer_callback(self):
        """Timer callback for the publishing the sine wave values."""
        try:
            amplitude = (
                self.get_parameter("amplitude").get_parameter_value().double_value
            )
            frequency = (
                self.get_parameter("frequency").get_parameter_value().double_value
            )

            if amplitude is None or frequency is None:
                self.get_logger().error(
                    f"Parameters {frequency=} {amplitude=} have no value."
                )
                return

            current_time = self.get_clock().now().to_msg()
            msg = Float64()
            msg.data = amplitude * np.sin(
                2 * np.pi * frequency * (current_time.sec + current_time.nanosec * 1e-9)
            )
            self._publisher.publish(msg)
            self.get_logger().debug("Published message to %s" % self.topic)
        except Exception as e:
            self.get_logger().error(f"Error: {e}")


def main(args=None) -> None:
    """Main entrypoint."""
    rclpy.init(args=args)
    generator = Generator()
    rclpy.spin(generator)
    generator.destroy_node()
    rclpy.shutdown()


if __name__ == "__main__":
    main()
