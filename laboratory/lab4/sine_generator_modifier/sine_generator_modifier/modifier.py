"""Node that modifies sine wave."""

import rclpy
from rclpy.node import Node
from rclpy.parameter import Parameter
from std_msgs.msg import Float64


class Modifier(Node):
    """Modifier node that scale the sine wave."""

    def __init__(self) -> None:
        """Initialize the class."""
        super().__init__("sine_wave_modifier")
        self.declare_parameters(
            namespace="",
            parameters=[
                ("amplitude", Parameter.Type.DOUBLE),
            ],
        )
        self.original_topic = "/signal"
        self.modified_topic = "/modified_signal"
        self.subscription = self.create_subscription(
            Float64,
            self.original_topic,
            self.listener_callback,
            10,
        )
        self.subscription  # prevent unused variable warning
        self.publisher = self.create_publisher(Float64, self.modified_topic, 10)

    def listener_callback(self, msg):
        """Callback for subscription to original signal and publishing."""
        amplitude = self.get_parameter("amplitude").get_parameter_value().double_value

        if amplitude is None:
            self.get_logger().error(f"Parameters {amplitude=} have no value.")
            raise ValueError(f"Parameters {amplitude=} have no value.")

        value = msg.data
        msg = Float64()
        msg.data = amplitude * value
        self.publisher.publish(msg)
        self.get_logger().debug("Published message to %s" % self.modified_topic)


def main(args=None) -> None:
    """Main entrypoint."""
    rclpy.init(args=args)
    modifier = Modifier()
    rclpy.spin(modifier)
    modifier.destroy_node()
    rclpy.shutdown()


if __name__ == "__main__":
    main()
