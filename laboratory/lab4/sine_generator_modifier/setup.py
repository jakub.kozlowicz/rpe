import glob
import os

import setuptools

package_name = "sine_generator_modifier"

setuptools.setup(
    name=package_name,
    version="0.0.0",
    packages=[package_name],
    data_files=[
        ("share/ament_index/resource_index/packages", ["resource/" + package_name]),
        ("share/" + package_name, ["package.xml"]),
        # Include all launch files.
        (
            os.path.join("share", package_name, "launch"),
            glob.glob(os.path.join("launch", "*.launch.py")),
        ),
        (os.path.join("share", package_name, "config"), glob.glob("config/*.yaml")),
    ],
    install_requires=["setuptools"],
    zip_safe=True,
    maintainer="Jakub Kozlowicz",
    maintainer_email="252865@student.pwr.edu.pl",
    description="Sine wave generator and modifier",
    license="MIT",
    tests_require=["pytest"],
    entry_points={
        "console_scripts": [
            "generator = sine_generator_modifier.generator:main",
            "modifier = sine_generator_modifier.modifier:main",
        ],
    },
)
