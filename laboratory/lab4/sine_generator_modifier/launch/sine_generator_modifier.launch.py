"""Launcher for the sine wave generator/modifier package."""

import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    generator_config = os.path.join(
        get_package_share_directory("sine_generator_modifier"),
        "config",
        "generator_params.yaml",
    )
    modifier_config = os.path.join(
        get_package_share_directory("sine_generator_modifier"),
        "config",
        "modifier_params.yaml",
    )
    return LaunchDescription(
        [
            Node(
                package="sine_generator_modifier",
                executable="generator",
                name="generator",
                parameters=[generator_config],
            ),
            Node(
                package="sine_generator_modifier",
                executable="modifier",
                name="modifier",
                parameters=[modifier_config],
            ),
        ]
    )
