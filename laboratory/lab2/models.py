from pydantic import BaseModel


class TemperatureData(BaseModel):
    sensor_name: str
    temperature: float


class HumidityData(BaseModel):
    sensor_name: str
    humidity: float
