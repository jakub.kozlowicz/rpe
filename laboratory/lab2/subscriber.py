import paho.mqtt.client as paho

from models import HumidityData, TemperatureData


def on_message(mosq, obj, msg):
    """Callback called when new message is received."""
    payload = msg.payload.decode("utf-8")
    topic = msg.topic
    _topic = topic.removeprefix("sensors/")
    sensor_type, *_ = _topic.split("/")

    if sensor_type == "temperature":
        data = TemperatureData.model_validate_json(payload)
    elif sensor_type == "humidity":
        data = HumidityData.model_validate_json(payload)
    else:
        data = None

    print(f"Topic: {topic} | Data: {data}")


def main() -> None:
    """Main entrypoint."""
    client = paho.Client()
    client.on_message = on_message
    client.on_connect = lambda client, userdata, flags, rc: print(f"Connected: {rc}")

    # Connect to broker
    client.connect("127.0.0.1", 1883, 60)

    # Subscribe to temp topic
    client.subscribe("sensors/#")
    try:
        client.loop_forever()
    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    main()
