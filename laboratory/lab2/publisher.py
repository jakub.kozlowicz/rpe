import random
import time

import click
import paho.mqtt.publish as publish

from models import HumidityData, TemperatureData


@click.command()
@click.option("--type", default="temperature", help="Type if the sensor.")
@click.option("--name", default="sensor1", help="Name of the sensor.")
def main(type: str, name: str) -> None:
    """Main entrypoint."""

    while True:
        try:
            value = random.randint(0, 100)
            if type == "temperature":
                paylod = TemperatureData(sensor_name=name, temperature=value)
            else:
                paylod = HumidityData(sensor_name=name, humidity=value)
            publish.single(
                topic=f"sensors/{type}/{name}",
                payload=paylod.model_dump_json(),
                hostname="127.0.0.1",
                port=1883,
            )
            time.sleep(1)
        except KeyboardInterrupt:
            break


if __name__ == "__main__":
    main()
